import React, { useState, useEffect } from 'react';
import { View, Text, ActivityIndicator, FlatList } from 'react-native';

const url = "https://jsonplaceholder.typicode.com/posts";

export default function Post() {
  const [response, setResponse] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    fetch(url)
      .then(res => res.json())
      .then((result) => {
        setResponse(result);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
        setError(error);
      });
  }, []);

  const getContent = () => {
    if (isLoading) {
      return (
        <View>
          <Text>Loading Data...</Text>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    if (error) {
      return (
        <View>
          <Text>Error: {error.message}</Text>
        </View>
      );
    }
    return (
      <FlatList
        data={response}
        renderItem={({ item }) => (
          <View>
            <Text>Title: {item.title}</Text>
          </View>
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    );
  };

  return (
    <View>
      {getContent()}
    </View>
  );
}