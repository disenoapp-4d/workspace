import React from 'react'; 
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
//import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import HomeScreen from './screens/HomeScreen';
import PokemonProfileScreen from './screens/PokemonProfileScreen';
import PokemonDetailScreen from './screens/PokemonDetailScreen';
import FavoritePokemonScreen from './screens/FavoritesPokemonScreen';
import PokemonRandomChoice from './screens/PokemonRandomChoiceScreen';


const Tab = createBottomTabNavigator();
//const Stack = createNativeStackNavigator();

/*
function MyStack(){
    return(
        <Stack.Navigator>
            <Stack.Screen name='HomeScreen' component={HomeScreen} />
            <Stack.Screen name="StackScreen" component={StackScreen} />
        </Stack.Navigator>
    );    
}
*/
function MyTab(){
    return(
        <Tab.Navigator>
            <Tab.Screen name='Home' component={HomeScreen} />
            <Tab.Screen name='Profiles' component={PokemonProfileScreen} />
            <Tab.Screen name='Favorites' component={FavoritePokemonScreen} />
            <Tab.Screen name='Random' component={PokemonRandomChoice} />
            <Tab.Screen name='Details' component={PokemonDetailScreen} />
        </Tab.Navigator>
    );
}

export default function Navigation(){
    return(
        <NavigationContainer>
            <MyTab />
        </NavigationContainer>
    );
}