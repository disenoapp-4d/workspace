import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ScrollView, ActivityIndicator, TextInput, TouchableOpacity } from 'react-native';

const url = "https://pokeapi.deno.dev/pokemon"; // API

export default function HomeScreen() {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [searchText, setSearchText] = useState('');

    useEffect(() => {
        fetch(url)
            .then(response => response.json())
            .then((result) => {
                setIsLoading(false);
                setData(result);
            })
            .catch((error) => {
                setIsLoading(false);
                setError(error);
            });
    }, []);

    const filterPokemon = (pokemonList, searchQuery) => {
        if (!searchQuery) {
            return pokemonList;
        }
        return pokemonList.filter(pokemon => pokemon.name.toLowerCase().includes(searchQuery.toLowerCase()));
    };

    const getContent = () => {
        if (isLoading) {
            return (
                <View style={styles.loadingContainer}>
                    <Text style={styles.textProps}>Loading Data...</Text>
                    <ActivityIndicator size="large" color="pink" />
                </View>
            );
        }
        if (error) {
            return <Text style={styles.textProps}>Error: {error}</Text>;
        }

        const filteredPokemon = filterPokemon(data, searchText);

        return (
            <ScrollView>
                {filteredPokemon.map((item, index) => (
                    <View key={index} style={styles.itemContainer}>
                        <Image style={styles.image} source={{ uri: item.imageUrl }} />
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => alert(`Name: ${item.name}`)}>
                            <Text style={styles.buttonText}>Name: {item.name}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => alert(`Genus: ${item.genus}`)}>
                            <Text style={styles.buttonText}>Genus: {item.genus}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => alert(`Types: ${item.types}`)}>
                            <Text style={styles.buttonText}>Types: {item.types}</Text>
                        </TouchableOpacity>
                    </View>
                ))}
            </ScrollView>
        );
    };

    return (
        <View style={styles.container}>
            <TextInput
                style={styles.searchInput}
                placeholder="Search Pokémon"
                onChangeText={text => setSearchText(text)}
                value={searchText}
            />
            {getContent()}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textProps: {
        fontSize: 32,
        fontFamily: 'Gill Sans',
    },
    itemContainer: {
        alignItems: 'center',
        marginBottom: 20,
    },
    image: {
        width: 100,
        height: 100,
    },
    searchInput: {
        backgroundColor: '#ffffff',
        paddingHorizontal: 10,
        paddingVertical: 8,
        fontSize: 16,
        borderRadius: 5,
        margin: 10,
    },
    button: {
        backgroundColor: '#6AB39F',
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#ffffff',
        textAlign: 'center',
    },
});
