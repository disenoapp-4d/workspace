import React from "react";
import { View,Text,StyleSheet } from "react-native";

export default function StackScreen(){
    return(
        <View>
            <Text style={styles.text}>Hola Stack Screen</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    text: {
      fontSize: 30,
    },
});
