const axios = require('axios')

const url = 'https://jsonplaceholder.typicode.com/users'

axios.post(url, {
    username: "Foo Bar",
    email: "foo@bar.com"
}).then(response => console.log(response.data))