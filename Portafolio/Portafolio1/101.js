console.log("Hola mundo")

var s = "Foo Bar"

//let x = 90

var y = 89

console.log(s)
console.log(x + y)

s = true

console.log(s)

var array = [1,2,3,4,5,"foo","bar",true,false,2.34,4.23]

var obj = {
    firs_name: "Foo", 
    last_name: "Bar", 
    age: 20, 
    city: "Tijuana", 
    status: true,
    arr: array
}

console.log(obj)
console.log(obj["firs_name"]) 
console.log(obj.last_name) 

for(let i=0; i<100; i+=5) {
    //console.log(i)
}

for(let i=0; i<array.length; i++) {
    console.log(array[i])
}

for(let i of array){
    console.log(i)
}

for(let key of Object.keys(obj)){ 
    console.log(key + ": " + obj[key])
}

for(let key in obj) { 
    console.log(key + ": " + obj[key])
}

var i = 1000
while(i <= 1000 && i >= 0) {
    console.log(i)
    i-=5
}

var k = 0
do{
    console.log(k)
    k += 5
} while(k < 1000)


var x = 9
var y = 8
if(x > y){
    console.log("Es 1")
} else {
    console.log("Is Not")
}

var opc = 8

switch(opc){
    case 1:
        console.log("I am the case 1")
    break;
    case 8: 
        console.log("I am the case 8")
    break;
    default:
        console.log("I am default")
    break;
}

var animal = "Salem"

var hello = (animal === "Salem") ? "That's my litle deamon ;)" : "That's not my cat :("

console.log(hello)

function foo(msg) {
    console.log(msg)
}
foo("Mensaje de prueba") // Agrega un mensaje al llamar foo()

var prism = function(l,w,h) {
    return l * w * h;
}
console.log("Volumen del prisma: "+prism(23,56,12))

function prisma(l) {
    return function(w) { 
        return function(h) {
            return l*w*h;
        }
    }
}
console.log("Volumen del prisma2: "+prisma(2)(3)(5))

var namedSum = function sum (a,b) {
    return a + b;
}
console.log(namedSum(1,3))

var anomSum = function (a,b) {
    return a + b;
}
console.log(anomSum(1,4))

fuu();
function fuu () {
    console.log("bar");
}

var say = function say(times) {
    say = undefined;
    if(times > 0) {
        console.log("Hello!");
        say(times - 1);
    }
}
var sayHello = say;
sayHello(2);

var saysay = say;
say = "oops";

saysay(2);