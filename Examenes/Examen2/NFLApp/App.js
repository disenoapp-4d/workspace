import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, SafeAreaView, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { fetchTodos } from './api';
import TodoList from './TodoList';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';

const Drawer = createDrawerNavigator();

const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState(null);

  useEffect(() => {
    fetchTodos()
      .then(setTodos)
      .catch(console.error)
      .finally(() => setLoading(false));
  }, []);

  const filteredTodos = todos.filter((todo) => {
    if (filter === 'completed') return todo.completed;
    if (filter === 'not-completed') return !todo.completed;
    return true;
  }).map((todo) => {
    switch (filter) {
      case 'ids':
        return { id: todo.id };
      case 'ids-titles':
        return { id: todo.id, title: todo.title };
      case 'ids-userids':
        return { id: todo.id, userId: todo.userId };
      case 'resolved-ids-userids':
        return todo.completed ? { id: todo.id, userId: todo.userId } : null;
      case 'unresolved-ids-userids':
        return !todo.completed ? { id: todo.id, userId: todo.userId } : null;
      default:
        return todo;
    }
  }).filter(Boolean);

  if (isLoading) return <ActivityIndicator />;

  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home" drawerContent={props => <DrawerContent {...props} setFilter={setFilter} />}>
        <Drawer.Screen name="Home">
          {() => (
            <SafeAreaView style={styles.container}>
              <View style={styles.header}>
                <Text style={styles.title}>NFL</Text>
              </View>
              <TodoList todos={filteredTodos} />
            </SafeAreaView>
          )}
        </Drawer.Screen>
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

const DrawerContent = ({ navigation, setFilter }) => {
  const handlePress = (filter) => {
    setFilter(filter);
    navigation.closeDrawer();
  };

  return (
    <View style={styles.drawerContent}>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('ids')}>
        <Text style={styles.drawerButtonText}>Todos (solo IDs)</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('ids-titles')}>
        <Text style={styles.drawerButtonText}>Todos (IDs y Títulos)</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('not-completed')}>
        <Text style={styles.drawerButtonText}>Pendientes sin resolver</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('completed')}>
        <Text style={styles.drawerButtonText}>Pendientes resueltos</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('ids-userids')}>
        <Text style={styles.drawerButtonText}>Todos (IDs y UserID)</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('resolved-ids-userids')}>
        <Text style={styles.drawerButtonText}>Pendientes resueltos (IDs y UserID)</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.drawerButton} onPress={() => handlePress('unresolved-ids-userids')}>
        <Text style={styles.drawerButtonText}>Pendientes sin resolver (IDs y UserID)</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#FFFFFF', // Cambiar color de fondo principal
  },
  header: {
    alignItems: 'center',
    marginBottom: 20,
  },
  title: {
    fontSize: 28,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#17161B', // Cambiar color de texto principal
  },
  drawerContent: {
    flex: 1,
    padding: 20,
    backgroundColor: '#CED0D4', // Cambiar color de fondo del cajón
  },
  drawerButton: {
    paddingVertical: 15,
    paddingHorizontal: 20,
    marginBottom: 10,
    backgroundColor: '#28272C', // Cambiar color de fondo de los botones del cajón
    borderRadius: 8,
    borderColor: '#'
  },
  drawerButtonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#F1F1F3', // Cambiar color de texto de los botones del cajón
    textAlign: 'center',
    
  },
});

export default App;