//TodoList.js
import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';

const TodoList = ({ todos }) => {
  return (
    <FlatList
      data={todos}
      keyExtractor={({ id }) => id.toString()}
      renderItem={({ item }) => (
        <View style={styles.listItem}>
          <Text style={styles.listItemText}>
            ID: {item.id}
            {item.title && `: ${item.title}`}
          </Text>
          {item.userId && <Text style={styles.userIdText}>UserID: {item.userId}</Text>}
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  listItem: {
    padding: 15,
    marginVertical: 8,
    backgroundColor: '#F1F1F3', // Cambiar color de fondo del ítem de la lista
    borderColor: '#B80C09', // Cambiar color del borde del ítem de la lista
    borderWidth: 4,
    borderRadius: 8,
  },
  listItemText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#17161B', // Cambiar color del texto del ítem de la lista
  },
  userIdText: {
    fontSize: 16,
    color: '#17161B', // Cambiar color del texto del UserID
    marginTop: 5,
  },
});

export default TodoList;