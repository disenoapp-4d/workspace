import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import ProfileScreen from './screens/Profile';
import MapScreen from './screens/Map';
import CarIfScreen from './screens/CarInf';

const Tab = createBottomTabNavigator();

const ButtomNavigator = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator>
        <Tab.Screen name="Profile" component={ProfileScreen} options={{ headerShown: false }}/>
        <Tab.Screen name="Map" component={MapScreen} options={{ headerShown: false }}/>
        <Tab.Screen name="Car" component={CarIfScreen} options={{ headerShown: false }}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default ButtomNavigator;

//SOLO ESTA DE PRUEBAS, ESTA OBSOLETO POR AHORA.
