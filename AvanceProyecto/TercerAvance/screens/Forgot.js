//LISTO

import React, { useState, useRef, useEffect } from 'react';
import { View, TextInput, Button, Text, StyleSheet, Dimensions, Animated, TouchableOpacity, Easing } from 'react-native';
import Svg, { Path, Defs, LinearGradient, Stop } from "react-native-svg";
import { StatusBar } from 'expo-status-bar';
const { width, height } = Dimensions.get('window');

const Forgot = ({ navigation }) => {
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');

    const fadeAnim = useRef(new Animated.Value(0)).current;

    useEffect(() => {
        Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true
        }).start();
    }, []);

    const handleForgotPassword = async () => {
        try {
            const response = await fetch('http://34.94.10.37:1337/api/auth/forgot-password', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: email,
                }),
            });
            const data = await response.json();
            if (response.ok) {
                setMessage('Se ha enviado un correo electrónico para restablecer la contraseña.');
            } else {
                setMessage('Hubo un error al enviar la solicitud.');
            }
        } catch (error) {
            console.error('Error:', error);
            setMessage('Hubo un error al enviar la solicitud.');
        }
    };

    return (
        <View style={styles.container}>
            <Svg width={width} height={324} fill="none" xmlns="http://www.w3.org/2000/svg" style={styles.containerSVG}>
                <Defs>
                    <LinearGradient
                        id="waveGradient"
                        x1={0}
                        y1={0}
                        x2={1}
                        y2={0}
                    >
                        <Stop offset="0" stopColor="#67cba0" stopOpacity="1" />
                        <Stop offset="0.5" stopColor="#7b92b2" stopOpacity="1" />
                        <Stop offset="1" stopColor="#67cba0" stopOpacity="1" />
                    </LinearGradient>
                    <Path
                        id="wavePath"
                        d="M-0.00240142 67.1055C26.3075 82.8311 53.3951 108.756 109.164 113.156C173.073 118.906 213.495 94.1812 273.997 83.1312C342.607 71.3133 399.891 85.4738 462.042 97.2318C531.548 110.519 607.684 121.75 667.999 100.815V324H-0.00240142V67.1055Z"
                    />
                    <Path
                        id="wavePathCopy"
                        d="M0 94.1055C11.3563 112.831 52.9564 141.756 107.099 146.156C166.077 151.906 205.278 142.181 265.209 131.131C329.06 119.313 400.546 106.474 461.735 118.232C531.288 131.519 604.009 149.75 664 128.815V324H0V94.1055Z"
                    />
                </Defs>
                <Path
                    d="M-0.00240142 67.1055C26.3075 82.8311 53.3951 108.756 109.164 113.156C173.073 118.906 213.495 94.1812 273.997 83.1312C342.607 71.3133 399.891 85.4738 462.042 97.2318C531.548 110.519 607.684 121.75 667.999 100.815V324H-0.00240142V67.1055Z"
                    fill="url(#waveGradient)"
                    fillOpacity="0.5"
                    transform="translate(0, -10)"
                />
                <Path
                    d="M0 94.1055C11.3563 112.831 52.9564 141.756 107.099 146.156C166.077 151.906 205.278 142.181 265.209 131.131C329.06 119.313 400.546 106.474 461.735 118.232C531.288 131.519 604.009 149.75 664 128.815V324H0V94.1055Z"
                    fill="url(#waveGradient)"
                    fillOpacity="0.5"
                    transform="translate(0, -10)"
                />
            </Svg>

            <Animated.View style={[styles.containerAnimated, { opacity: fadeAnim }]}>
                <Text style={styles.title}>Restablecer Contraseña</Text>
                <TextInput
                    placeholder="Correo Electrónico"
                    style={styles.textInput}
                    onChangeText={setEmail}
                    value={email}
                />
                <TouchableOpacity onPress={handleForgotPassword} activeOpacity={0.8}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Enviar Solicitud</Text>
                    </View>
                </TouchableOpacity>
                <Text style={styles.message}>{message}</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.link}>Volver al Login</Text>
                </TouchableOpacity>
                <StatusBar style="auto" />
            </Animated.View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f1f1f1',
    },
    containerAnimated: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerSVG: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        transform: [{ rotate: '180deg' }],
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
        color: '#7b92b2',
    },
    textInput: {
        width: '80%',
        height: 50,
        padding: 10,
        borderWidth: 1,
        borderColor: 'gray',
        marginBottom: 20,
        borderRadius: 10,
        backgroundColor: '#fff',
    },
    button: {
        backgroundColor: '#67cba0',
        width: '80%',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
    },
    message: {
        marginTop: 20,
        color: 'red',
    },
    link: {
        marginTop: 20,
        color: 'blue',
        textDecorationLine: 'underline',
    },
});

export default Forgot;
