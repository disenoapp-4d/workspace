import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Animated, Dimensions, useWindowDimensions, TextInput, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

const CargasComb = ({ navigation, route }) => {
  const ip = '34.94.10.37';
  const { token } = route.params || { token: null };
  const [userData, setUserData] = useState({});
  const [vehicleID, setVehicleID] = useState(null);
  const [idRutaNoFinalizada, setIdRutaNoFinalizada] = useState(null);
  const [refuelsData, setRefuelsData] = useState(null);
  const [fadeAnim] = useState(new Animated.Value(0));
  const [triggerRefuelsDataFetch, setTriggerRefuelsDataFetch] = useState(false);
  // Estado para los campos del formulario
  const [numFolio, setNumFolio] = useState('');
  const [litrosCargados, setLitrosCargados] = useState('');
  const [costo, setCosto] = useState('');
  const [kilometraje, setKilometraje] = useState('');
  const [showItems, setShowItems] = useState(true); // Estado para controlar la visibilidad de la lista de ítems


  const handleSubmit = async () => {
    setShowItems(false)
    const fechaActual = new Date();
    const fecha = fechaActual.toISOString();
    const num = 16;
    //console.log({num, numFolio, litrosCargados, costo, kilometraje, fecha, vehicleID });
    try {
      if (!vehicleID) {
        console.error('ID de vehículo no disponible');
        return;
      }
      const response = await fetch(`http://${ip}:1337/api/cargas-combustibles?populate=*`, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: {
            numFolio: numFolio,
            litrosCargados: +litrosCargados,
            fecha: fecha,
            kilometraje: kilometraje,
            comentarios: "Sin Comentarios",
            costoTotal: +costo,
            vehiculos: vehicleID + ''
          }
        }),
        redirect: "follow"
      });
      console.log(response.status);
      if (!response.ok) {
        throw new Error('Código de estado:', response.status);
      }
      const data = await response.json();
      //console.log('Datos enviados con éxito:', data);
    } catch (error) {
      //console.error('Error:', error);
    }
    setTriggerRefuelsDataFetch(prevState => !prevState);
  };

  // Obtén el ancho de la pantalla
  const { width } = Dimensions.get('window');

  useEffect(() => {
    if (!token) {
      console.error('Token de autenticación no proporcionado');
      return;
    }
  }, [])

  useEffect(() => {
    const fetchUserData = async () => {
      try {
        const response = await fetch(`http://${ip}:1337/api/users/me?populate=*`, {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });
        if (response.ok) {
          const data = await response.json();
          setUserData(data);
          Animated.timing(
            fadeAnim,
            {
              toValue: 1,
              duration: 1000,
              useNativeDriver: true
            }
          ).start();
        } else {
          console.error('Error al obtener los datos del usuario');
        }
      } catch (error) {
        console.error('Error:', error);
      }
    };

    fetchUserData();
  }, [token]);

  useEffect(() => {
    if (userData.rutas) {
      const filtroRuta = userData.rutas.find(ruta => ruta.finalizado === false)
      setIdRutaNoFinalizada(filtroRuta?.id); // Usando optional chaining
    }
  }, [userData])

  useEffect(() => {
    const fetchRouteData = async () => {
      if (idRutaNoFinalizada != null) {
        try {
          const routeResponse = await fetch(`http://${ip}:1337/api/rutas/${idRutaNoFinalizada}/?populate=*`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          if (routeResponse.ok) {
            const data = await routeResponse.json();
            setVehicleID(data.data.attributes.vehiculo.data[0].id);
          } else {
            console.error('Error al obtener los datos de la ruta');
          }
        } catch (error) {
          console.error('Error:', error);
        }
      }
    };
    fetchRouteData();
  }, [idRutaNoFinalizada, token])

  useEffect(() => {
    const fetchRefuelsData = async () => {
      if (vehicleID) {
        try {
          const refuelsResponse = await fetch(`http://${ip}:1337/api/cargas-combustibles?filters[vehiculos][id]=${vehicleID}&populate=**`, {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          });
          if (refuelsResponse.ok) {
            const data = await refuelsResponse.json();
            setRefuelsData(data);
          } else {
            console.error('Error al obtener los datos de las cargas de combustible');
          }
        } catch (error) {
          console.error('Error:', error);
        }
      }
    };

    fetchRefuelsData();
  }, [vehicleID, triggerRefuelsDataFetch])

  function formatMyDate() {
    return new Date(userData.updatedAt).toLocaleDateString('en-GB');
  }

  const { height } = useWindowDimensions();
  const containerHeight = height * 0.7;


  return (
    <LinearGradient colors={['#7b92b2', '#fff']} style={styles.background}>
      <View style={[styles.mainContainer, styles.container]}>
        <View style={styles.header}>
          <View style={[styles.button]}>
            <Ionicons name="map-outline" color={'#fff'} size={22} />
          </View>
          <Text style={{ fontWeight: 'bold', fontSize: 30, color: '#fff' }} >Cargas de combustible</Text>
        </View>
        <View style={styles.contentContainer}>
          {showItems && (
            <ScrollView contentContainerStyle={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'space-between',
            }}>
              {refuelsData && refuelsData.data.map((item) => (
                <View key={item.id} style={styles.itemContainer}>
                  <Text style={styles.itemText}>Num. Folio: <Text style={styles.itemTextNormal}>{item.attributes.numFolio}</Text></Text>
                  <Text style={styles.itemText}>Litros cargados: <Text style={styles.itemTextNormal}>{item.attributes.litrosCargados}</Text></Text>
                  <Text style={styles.itemText}>Costo: <Text style={styles.itemTextNormal}>${item.attributes.costoTotal} MXN</Text></Text>
                  <Text style={styles.itemText}>Fecha: <Text style={styles.itemTextNormal}>{formatMyDate(item.attributes.fecha)}</Text></Text>
                </View>
              ))}
            </ScrollView>
          )}
          <View style={styles.formContainer}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: '#fff' }}>Nueva Carga: </Text>
            <View style={styles.inputRow}>
              <View style={styles.inputGroup}>
                <Text style={styles.label}>Num. Folio</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Num. Folio"
                  value={numFolio}
                  onChangeText={setNumFolio}
                  onFocus={() => setShowItems(false)}
                  onBlur={() => setShowItems(true)}
                />
              </View>
              <View style={styles.inputGroup}>
                <Text style={styles.label}>Litros cargados</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Litros cargados"
                  value={litrosCargados}
                  onChangeText={setLitrosCargados}
                  onFocus={() => setShowItems(false)}
                  onBlur={() => setShowItems(true)}
                />
              </View>
            </View>
            <View style={styles.inputRow}>
              <View style={styles.inputGroup}>
                <Text style={styles.label}>Costo</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Costo"
                  value={costo}
                  onChangeText={setCosto}
                  onFocus={() => setShowItems(false)}
                  onBlur={() => setShowItems(true)}
                />
              </View>
              <View style={styles.inputGroup}>
                <Text style={styles.label}>Kilometraje (km)</Text>
                <TextInput
                  style={styles.textInput}
                  placeholder="Kilometraje"
                  value={kilometraje}
                  onChangeText={setKilometraje}
                  onFocus={() => setShowItems(false)}
                  onBlur={() => setShowItems(true)}
                />
              </View>
            </View>
            <View style={{ display: 'flex', width: '100%', alignItems: 'center', }}>
              <TouchableOpacity onPress={handleSubmit} style={styles.logoutButton}>
                <Text style={styles.logoutText}>Crear</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </LinearGradient>
  );
};


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginVertical: 70,
    marginHorizontal: 40,
  },
  header: {
    flexDirection: 'row',
  },
  routeInfoContainer: {
    marginVertical: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#7b92b2',
    paddingBottom: 10,
    marginBottom: 10,
  },
  subTitle: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: '20',
  },
  map: {
    width: '100%',
    height: '60%',
    marginTop: 10,
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 25,
    borderWidth: 4,
    borderColor: '#0792b3',
    backgroundColor: '#0792b3',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    marginRight: 10
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#000',
    borderRadius: 5,
    padding: 10,
  },
  itemContainer: {
    marginBottom: 10,
    padding: 10,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  background: {
    flex: 1,
    width: '100%',
  },
  titulo: {
    fontSize: 50,
    color: '#7b92b2',
    fontWeight: 'bold',
    paddingHorizontal: 20,
  },
  subTitle: {
    fontSize: 20,
    color: 'gray',
    paddingBottom: 30
  },
  textInput: {
    padding: 10,
    paddingStart: 30,
    width: '80%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
  errorMessage: {
    fontSize: 14,
    color: 'red',
    marginTop: 20,
  },
  itemsContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  itemContainer: {
    marginVertical: 10,
    backgroundColor: '#7b92b2',
    padding: 15,
    borderRadius: 20,
    marginRight: 10,
  },
  itemText: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fff',
  },
  itemTextNormal: {
    fontWeight: 'normal',
  },
  formContainer: {
    borderTopWidth: 2,
    borderTopColor: '#7b92b2',
    marginTop: 20,
    padding: 10,
    borderRadius: 10,
    width: '100%',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    paddingLeft: 10,
  },
  inputGroup: {
    width: "48%",
  },
  label: {
    fontWeight: 'bold',
    marginBottom: 5,
  },
  contentContainer: {
    height: '87%',
    display: 'flex',
    flexDirection: 'column',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  textInput: {
    padding: 10,
    paddingStart: 30,
    width: '100%',
    height: 50,
    marginTop: 10,
    borderRadius: 30,
    backgroundColor: '#fff',
  },
  inputRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  logoutButton: {
    width: 150,
    backgroundColor: '#7b92b2',
    paddingVertical: 15,
    borderRadius: 20,
    marginTop: 20,
    paddingHorizontal: 30,
  },
  logoutText: {
    fontSize: 18,
    fontWeight: 'bold',
    color:
      '#fff',
    textAlign: 'center',
  },
});

export default CargasComb;
